import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { RealmsSession } from './realms-session';

@Injectable({
  providedIn: 'root'
})
export class LoginserviceService {

  constructor(private authModuleService: OAuthService) { }

  public login():void{
    this.authModuleService.initImplicitFlowInternal();
  }
  public logout():void{
    this.authModuleService.logOut();
  }

  public getislogin():boolean{
    return (this.authModuleService.hasValidIdToken()&&this.authModuleService.hasValidAccessToken())
  }

  public getAccessData():RealmsSession{
    return JSON.parse(atob(this.authModuleService.getAccessToken().split('.')[1]));
  }
}
