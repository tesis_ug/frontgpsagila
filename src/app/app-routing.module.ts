import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AppComponent } from './app.component';
import { NavbarComponent } from './home/navbar/navbar.component';
import { ConsultRoutesComponent } from './home/consult-routes/consult-routes.component';

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [{ path: '', component: HomeComponent },
  {
    path:'consult', component: ConsultRoutesComponent
  }  
  ],
  },
  {
    path: '',
    component: NavbarComponent,
    outlet: "sidebar"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
