import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { OAuthService } from 'angular-oauth2-oidc';
import { LoginserviceService } from '../../loginservice.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  items: MenuItem[];

  constructor(public loginserviceService: LoginserviceService ) {
    
    this.items = [
      {
        label: 'Gestion de Usuarios',
        items: [
          {
            label: 'Nuevo Usuario',
            icon: 'pi pi-fw pi-plus',
          },
          { label: 'Control de sesiones', routerLink: ['/consult'] },
          {
            label: 'Nuevo rol',
            icon: 'pi pi-fw pi-plus',
          },
        ],
      },
      {
        label: 'Monitoreo',
        icon: 'pi pi-fw pi-search',
        items: [
          {
            label: 'Visualizacion en tiempo real',
            icon: 'pi pi-fw pi-refresh',
            routerLink: ['/consult'],
          },
        ],
      },
    ];
  }

  ngOnInit(): void {
   
  }

}
