import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultRoutesComponent } from './consult-routes.component';

describe('ConsultRoutesComponent', () => {
  let component: ConsultRoutesComponent;
  let fixture: ComponentFixture<ConsultRoutesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultRoutesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultRoutesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
