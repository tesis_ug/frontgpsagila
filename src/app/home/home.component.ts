import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  images: any[]=[
    {
      previewImageSrc: "assets/image/0.jpg",
      thumbnailImageSrc: "assets/image/0.jpg",
      alt: "Description for Image 1",
      title: "Title 1"
    },
    {
      previewImageSrc: "assets/image/2.jpg",
      thumbnailImageSrc: "assets/image/2.jpg",
      alt: "Description for Image 1",
      title: "Title 1"
    },
    {
      previewImageSrc: "assets/image/3.jpg",
      thumbnailImageSrc: "assets/image/3.jpg",
      alt: "Description for Image 1",
      title: "Title 1"
    }
  ];

  responsiveOptions:any[] = [
    {
      breakpoint: '1024px',
      numVisible: 5,
    },
    {
      breakpoint: '768px',
      numVisible: 3
    },
    {
      breakpoint: '560px',
      numVisible: 1
    }
  ];

}
