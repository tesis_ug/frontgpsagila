import { Component } from '@angular/core';
import { AuthConfig, NullValidationHandler, OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'agillasanchezfront';
  constructor(private oathservice: OAuthService){
    this.configure();
  }
  authCodeFlowConfig: AuthConfig = {
    issuer: 'https://lemur-9.cloud-iam.com/auth/realms/agilasanchez',
    redirectUri: window.location.origin,
    clientId: 'agilafront',
    responseType: 'code',
    scope: 'openid profile email offline_access',
    showDebugInformation: true,
  };

  configure():void{
    this.oathservice.configure(this.authCodeFlowConfig);
    this.oathservice.tokenValidationHandler = new NullValidationHandler();
    this.oathservice.setupAutomaticSilentRefresh();
    this.oathservice.loadDiscoveryDocument().then(()=> this.oathservice.tryLogin());
  }


}



/*
{
  "realm": "agilasanchez",
  "auth-server-url": "https://lemur-9.cloud-iam.com/auth/",
  "ssl-required": "external",
  "resource": "agilafront",
  "public-client": true,
  "verify-token-audience": true,
  "use-resource-role-mappings": true,
  "confidential-port": 0
}
*/

