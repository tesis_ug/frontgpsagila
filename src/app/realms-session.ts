export interface RealmsSession {
    exp?: number;
    iat?: number;
    auth_time?: number;
    jti?: string;
    iss?: string;
    aud?: string;
    sub?: string;
    typ?: string;
    azp?: string;
    nonce?: string;
    session_state?: string;
    acr?: string;
    "allowed-origins"?: string[];
    realm_access?: RealmAccess;
    resource_access?: ResourceAccess;
    scope?: string;
    sid?: string;
    email_verified?: boolean;
    name?: string;
    preferred_username?: string;
    given_name?: string;
    family_name?: string;
    email?: string;
  }
  
  export interface RealmAccess {
    roles?: string[];
  }
  
  export interface ResourceAccess {
    AgilaSanchez?: RealmAccess;
    account?: RealmAccess;
  }
  
  // Converts JSON strings to/from your types
  export class Convert {
    public static toRealmsSession(json: string): RealmsSession {
      return JSON.parse(json);
    }
  
    public static realmsSessionToJson(value: RealmsSession): string {
      return JSON.stringify(value);
    }
  }
  
  
  